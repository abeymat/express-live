/*------------------------------------------------------------------------------
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is levelonelabs.com code.
 * The Initial Developer of the Original Code is Level One Labs. Portions
 * created by the Initial Developer are Copyright (C) 2001 the Initial
 * Developer. All Rights Reserved.
 *
 *         Contributor(s):
 *             Scott Oster      (ostersc@alum.rpi.edu)
 *             Steve Zingelwicz (sez@po.cwru.edu)
 *             William Gorman   (willgorman@hotmail.com)
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable
 * instead of those above. If you wish to allow use of your version of this
 * file only under the terms of either the GPL or the LGPL, and not to allow
 * others to use your version of this file under the terms of the NPL, indicate
 * your decision by deleting the provisions above and replace them with the
 * notice and other provisions required by the GPL or the LGPL. If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under the terms of any one of the NPL, the GPL or the LGPL.
 *----------------------------------------------------------------------------*/

package com.levelonelabs.aimbot;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Stack;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


import com.levelonelabs.aim.AIMAdapter;
import com.levelonelabs.aim.AIMBuddy;
import com.levelonelabs.aim.AIMClient;
import com.levelonelabs.aim.AIMGroup;
import com.levelonelabs.aim.AIMSender;
import com.levelonelabs.aim.XMLizable;
//import java.util.Timer;
//import java.util.TimerTask;

import com.targetx.ExpressDbMgr;


/**
 * The main Aimbot that uses the aim package to communicate with AIM and
 * provides services via AIM through various bot modules that perform action
 * when clients make specific queries to the screename registered by this bot.
 * 
 * @author Scott Oster (ostersc@alumn.rpi.edu)
 * 
 * @created January 10, 2002
 */
public class AIMBot extends AIMAdapter {
	public static final String ROLE_USER = "User";
	public static final String ROLE_ENEMY = "Enemy";
	public static final String ROLE_ADMINISTRATOR = "Administrator";

	private static final String PERSISTANCE_FILENAME = "persistance.xml";
	private static Logger logger = Logger.getLogger(AIMBot.class.getName());
	/** A handle to the outgoing AIM connection */
	protected AIMSender aim;
	private String username;
	private String password;
	private boolean autoAdd = false;
	private Hashtable services;
	private Hashtable groups;
	private List modules;
	private boolean enforceUser;
	private String nonUserResponse;

    public String  sug_module;
	public Integer im_id; // This holds the Im_id  
    public String  im_name;
	public Stack   ExpressStack = new Stack();
	public String startTime; // sign on time
	public String admin="";// variable to hold admin screen name
	public String defMessage="";// variable to hold default message
	public Integer time_delay=10000; // Maximum delay counselor takes to respond for a chat request
	
	
  public ExpressDbMgr test = new ExpressDbMgr();
  

	
	/**
	 * Constructor for the AIMBot object
	 */
	public AIMBot() {
		services = new Hashtable();
		groups = new Hashtable();
		modules = new ArrayList();
		
		
		
	}


	/**
	 * Add the specified username as an admin of the bot, if it is not already
	 * 
	 * @param admin
	 */
	 private void verifyAdmin(String admin) {
		if (admin.equals("")) {
			return;
		}
		AIMBuddy adminBuddy = aim.getBuddy(admin);
		if (adminBuddy == null) {
			adminBuddy = new AIMBuddy(admin);
		}
		if (!adminBuddy.hasRole(AIMBot.ROLE_USER)) {
			adminBuddy.addRole(AIMBot.ROLE_USER);
			logger.info("Adding " + AIMBot.ROLE_USER + " role to " + admin);
		}
		if (!adminBuddy.hasRole(AIMBot.ROLE_ADMINISTRATOR)) {
			adminBuddy.addRole(AIMBot.ROLE_ADMINISTRATOR);
			logger.info("Adding " + AIMBot.ROLE_ADMINISTRATOR + " role to " + admin);
		}
		if (aim.getBuddy(adminBuddy.getName()) == null) {
			aim.addBuddy(adminBuddy);
			logger.info("Adding " + admin + " as an admin.");
		}
	}


	/**
	 * Start up the bot.
	 * 
	 * @param args
	 *            The command line arguments
	 */
	public static void main(String[] args) {
	
	
	    if (args.length != 1) {
			System.err.println("USAGE: username password newusername");
			System.exit(-1);
		}

		//this.username = args[0];
		
	

		AIMBot bot = new AIMBot();
		
		
		bot.init(args[0]);
		//bot.init(propsFile);
	}

	public void init(String username) 
	{
	  String userP ="";
	  String sql_str="";
	  String passP="";
	  String autoaddP="";
	  String enforceUserP="";
	  String profileP="";
	  String nonUserResponseP="";
      String loglevel="";
      String logFile="";    
      //String version = @version@;
    
      this.test.init();
		
      // logger.info("version -- "+version);
	
	
	  sql_str = "select B.im_id,B.password,auto_add,enforce_user,profile,non_user_response,log_level,log_file,now(),time_delay"+
	            " from im_clients as A ,im_client_bots as B"+
				" where A.client_id = B.client_id and B.screen_name ='"+username+"' and B.active = 'Y'";
	  this.test.RunSql(sql_str);
	  
	  if(this.test.totalNumRows < 1)
	  {
    	  System.err.println("Sorry: The screen Name is not active at the moment");
    	  System.exit(-1);
      }	  
	  userP = username;
		
      try 
      {   
           this.test.rs.next();
           if( this.test.rs.getInt(1) > 0)
           {
           	 
                   setImId(this.test.rs.getInt(1));
                   passP        = this.test.rs.getString(2);
			       autoaddP     = "true";
			       enforceUserP = "false";
			       profileP     = this.test.rs.getString(5);
			       nonUserResponseP = this.test.rs.getString(6);
                   loglevel     = this.test.rs.getString(7);
		           logFile      =  this.test.rs.getString(8)+username+"_aimbot.log.xml";
				   //this.test.rs.getString(8);
				   this.startTime = this.test.rs.getString(9);
				   this.time_delay = this.test.rs.getInt(10);
           }
        }
        catch (Exception e) 
        {   
                 
                  logger.info("Error in AIM Bot -- "+sql_str);
                  e.printStackTrace();
        }
	
	
	   // get the im default message
	   sql_str =  "select  keyword,key_desc,seq_no FROM im_menu as A where A.im_id = " +
        	       this.getImId() + " and    A.keyword ='def'  and parent_id =0 order by seq_no";
	   this.test.RunSql(sql_str);
	   if(this.test.totalNumRows > 0)
	   {
	       try 
           {   
              this.test.rs.next();
			  this.defMessage =  this.test.rs.getString(2);
	       }
		   catch (Exception e) 
		   {
		      logger.info("Error in AIM Bot -- "+sql_str);
		   }
	   }
	   
	     userP = userP.replaceAll(" ", "");
	     
	       if (getImId() < 1) 
	       {
						System.err.println("here: username password newusername");
						System.exit(-1);
			}
	      
		   setupAIM(userP, passP, profileP, nonUserResponseP, autoaddP, enforceUserP);

		   //Setup the Logger
		   setupLogger(loglevel, logFile);
		   
		   

  
    
    String propsFile = System.getProperty("config.file", "bot.properties");
    
    Properties props = null;
		try {
			InputStream is = ClassLoader.getSystemResourceAsStream(propsFile);
			props = new Properties();
			props.load(is);
		} catch (Exception e) {
			logger.severe("Failed to load props file (" + propsFile + ")!");
			logger.severe("You must configure bot.properties, or add -Dconfig.file=<CONFIGFILE> to use a different one(must be on classpath).");
			e.printStackTrace();
			System.exit(-1);
		}

    
		//load in the mod names
		List modNames = new ArrayList();
		for (int i = 0; i < props.size(); i++) {
			if (props.containsKey("mod." + i)) {
				String modName = props.getProperty("mod." + i);
				modNames.add(modName);
			}
		}
		loadModules(modNames);
     
		expDePersist();
		//depersist();
		
	  // get the administarator
	  sql_str = "select screen_name"+
	            " from im_buddies where im_id = "+this.getImId() +" and role_id = 1";
	  this.test.RunSql(sql_str);
	  
	  if(this.test.totalNumRows < 1)
	  {
	     System.err.println("Sorry: No administrator found in the buddies table");
    	  System.exit(-1);
	  }
		
      try 
      {
           this.test.rs.next();
           if( this.test.rs.getString(1).length() > 0)
           {
               this.admin = this.test.rs.getString(1);
              
           }
        }
        catch (Exception e) 
        {   
                 
                  logger.info("Error in menu module -- "+sql_str);
        }
		
		
		//String admin = props.getProperty("bot.admin", "").trim();

		//remove whitespace from username, as AOL ignores it
		admin = admin.replaceAll(" ", "");
		verifyAdmin(this.admin);
		aim.signOn();
	}

	private void setupAIM(String user, String pass, String profile, String nonUserResponse, String autoAddUsers,String enforceUser) 
	{
		this.nonUserResponse = nonUserResponse;
		if ((user == null) || (pass == null) || user.trim().equals("") || pass.trim().equals("")) {
			logger.severe("ERROR: invalid username or password.");
			System.exit(-1);
		} else 
		{
			this.username = user;
			this.password = pass;
			//this.test.init();
			this.getImDetails();
			
			
		}

		//check for autoadd
		this.autoAdd = false;
		if (autoAddUsers.equalsIgnoreCase("true")) {
			this.autoAdd = true;
		}

		//check for enforceUser
		this.enforceUser = true;
		if (enforceUser.equalsIgnoreCase("false")) {
			this.enforceUser = false;
		}

		aim = new AIMClient(username, password, profile, nonUserResponse, this.autoAdd);
		aim.addAIMListener(this);
	}


	public void setupLogger(String logLevel, String logFilename) 
	{
	    //logger.severe("ERROR: trace " +logLevel + "--"+logFilename );
		Level level = null;
		try {
			level = Level.parse(logLevel);
		} catch (Exception e) {
			System.err.println("ERROR parsing log level, defaulting to INFO");
			level = Level.INFO;
		}

		try {
			Handler fh = new FileHandler(logFilename);
			Logger.getLogger("").addHandler(fh);

			Logger.getLogger("").setLevel(level);
			Handler[] handlers = Logger.getLogger("").getHandlers();
			for (int i = 0; i < handlers.length; i++) 
			{
				handlers[i].setLevel(level);
			}
		} catch (Exception e) {
			logger.severe("ERROR: unable to attach FileHandler to logger!");
			e.printStackTrace();
		}
	}


	/**
	 * Returns the AIM username of the bot.
	 * 
	 * @return the AIM username of the bot.
	 */
	public String getUsername() {
		return username;
	}


	/**
	 * Gets the specified group
	 * 
	 * @param groupName
	 * 
	 * @return The group value
	 */
	public AIMGroup getGroup(String groupName) {
		return (AIMGroup) this.groups.get(groupName);
	}


	/**
	 * Gets an enum of the groups of the AIMBot
	 * 
	 * @return The groups enumeration
	 */
	public Enumeration getGroups() {
		return this.groups.elements();
	}

	public void handleMessage(AIMBuddy buddy, String request,String module) 
	{
	   
	   if(module != null)
	   {
	     sug_module = module;
		 buddy.setModule(module);
	   }
	
	   this.handleMessage(buddy,request);
    }
	/**
	 * Will call the appropriate bot module to service the request.
	 * 
	 * @param buddy
	 *            the buddy making the request
	 * @param request
	 *            the text of the request
	 */
	public void handleMessage(AIMBuddy buddy, String request) 
	{
	
	    String sql_str;
		if (buddy != null) {
			logger.info(buddy.getName() + " said: " + request);
		} else {
			logger.info("Ignoring request:" + request + " from null buddy.");
			return;
		}

		if (buddy.hasRole(ROLE_ENEMY)) {
			logger.info("Ignoring request:" + request + " from Enemy:" + buddy.getName() + " and attempting to warn");
			retaliate(buddy);
			return;
		}
		if (this.enforceUser && !buddy.hasRole(ROLE_USER)) {
			//if we are enforcing the user list and the buddy isnt a user,
			// ignore the request and send the non-user response
			logger.info("Ignoring request:" + request + " from buddy that isn't a user, sending non user response.");
			if (this.nonUserResponse != null && !this.nonUserResponse.trim().equals("")) {
				aim.sendMessage(buddy, this.nonUserResponse);
			}
			return;
		}
		StringTokenizer stok = new StringTokenizer(request, " ");
		String keyword = "";
		String orig_keyword = "";
		if (stok.hasMoreTokens()) 
		{
			keyword = stok.nextToken().toLowerCase().trim();
			orig_keyword = keyword;
		}
	
		if (buddy.getModule() != null)   
		{  
		   keyword = "";
		   keyword = buddy.getModule();
		} 
		else
		{
		  keyword    = "start"; 
		  buddy.setModule(keyword);
		}
		
	 logger.info("----------- test -------------");
		 
		
		// || orig_keyword.equals("hello")
//		if(orig_keyword.equals("start") || orig_keyword.equals("menu") || orig_keyword.equals("hi") || orig_keyword.equals("hello") ) 
		if(request.trim().toLowerCase().equals("start") || request.trim().toLowerCase().equals("menu") || request.trim().toLowerCase().equals("hi") || request.trim().toLowerCase().equals("hello") ) 
		{
		
		    if(buddy.getModule() != "proxy"  & buddy.getModule() != "tell" )
			{
    		  keyword    = "start"; 
    		  buddy.setModule(keyword);
			}
		}
		else if (request.toLowerCase().trim().startsWith("sys/")) 
		{
			  if(!buddy.getName().trim().equals(this.admin))
			  {
			    logger.info("this is not admin");
				aim.sendMessage(buddy, "Sorry,sys commands are only for admin");
				return;
			  }
			  
			 if (request.toLowerCase().trim().equals("sys/signoff")) 
		     {		
    			aim.signOff();
    			System.exit(-1);
		     }
			 keyword    = "sys"; 
		}
		
		 
		/*
		else if (request.toLowerCase().trim().equals("sys/signoff")) 
		{
			
			aim.signOff();
			System.exit(-1);
			
		}
		*/
		
		/*
		try 
        {   
            if(this.test.db_conn.isClosed())
    		{
    		   this.test.init();
    		}   
        }
		catch (Exception e) 
        {              
             
              e.printStackTrace();
        }
		
		*/
		int bytes_sent = request.length()*8;
		String im_client = "";
		// log the activities
		sql_str = "insert into im_activity_log(im_id,buddy_screen_name,screen_name,keyword,status_code,bytes_sent,reference,im_client)values("+
	            this.getImId()+",'"+ this.getScreenName() + "','"+ buddy.getName()+"','"+request+"','200',"+bytes_sent+",'"+keyword+"','"+im_client+"')";

		  
      try 
      {   
           this.test.RunSql(sql_str);
      }
      catch (Exception e) 
      {              
                  logger.info("Error in AIM Bot -- "+sql_str);
                  e.printStackTrace();
      }
		
		
		
		
		BotModule mod = (BotModule) services.get(keyword);
		if (mod != null) 
		{
			logger.info("Request(" + keyword + ") being serviced by: " + mod.getName());
			mod.performService(buddy, request);
		} 
		else 
		{
			mod = (BotModule) modules.get(0);
			if (mod != null) 
			{
				logger.info("Default Request (" + keyword + ") being serviced by: " + mod.getName());
				mod.performService(buddy, request);
			} else 
			{
				logger.severe("Couldn't find mod to service request(" + keyword + ").");
			}
		}
		
		
		
		
	}


	/**
	 * Called when AIM encounters an error
	 * 
	 * @param error
	 *            the error code
	 * @param message
	 *            decsriptive text describing the error
	 */
	public void handleError(String error, String message) {
		logger.severe("ERROR(" + error + "): " + message);
	}


	/**
	 * Handle being warned from others. Mark them with the Enemy role and warn
	 * them back after a witty response.
	 * 
	 * @param enemy
	 *            the buddy that warned us
	 * @param amount
	 *            the current warning level
	 */
	public void handleWarning(AIMBuddy enemy, int amount) {
		if ((enemy == null) || (enemy.getName().equals("anonymous"))) {
			logger.info("AIMBot UNDER ATTACK!: anonymous raised warning to " + amount + " ***");
			return;
		}
		logger.info("AIMBot UNDER ATTACK!: " + enemy.getName() + " raised warning to " + amount + " ***");

		retaliate(enemy);
	}


	/**
	 * Warn a user, and mark them as an Enemy. If they were already an Enemy,
	 * ban them.
	 * 
	 * @param enemy
	 */
	private void retaliate(AIMBuddy enemy) {
		if (!enemy.hasRole(ROLE_ENEMY)) {
			enemy.addRole(ROLE_ENEMY);
			aim.sendMessage(enemy,
				"In the End, we will remember not the words of our enemies, but the silence of our friends");
		} else {
			aim.banBuddy(enemy);
		}
		aim.sendWarning(enemy);
	}


	/**
	 * Adds a Group
	 * 
	 * @param group
	 *            The feature to be added to the Group attribute
	 */
	public void addGroup(AIMGroup group) {
		this.groups.put(group.getName(), group);
	}


	/**
	 * Removes the specified group
	 * 
	 * @param group
	 */
	public void removeGroup(AIMGroup group) {
		this.groups.remove(group.getName());
	}


	/**
	 * All bot modules will call this and pass a reference to themselves and an
	 * ArrayList containing the keywords they want to listen for
	 * 
	 * @param mod
	 *            the module
	 */
	protected void registerModule(BotModule mod) {
		this.modules.add(mod);
		ArrayList servicesList = mod.getServices();
		if (servicesList != null) {
			for (int i = 0; i < servicesList.size(); i++) {
				registerService((String) servicesList.get(i), mod);
			}
		}
	}


	/**
	 * Load BotModules and register them
	 * 
	 * @param modNames
	 *            List of names to load
	 */
	private void loadModules(List modNames) {
		//wipe the slate clean
		services = new Hashtable();
		modules = new ArrayList();
 
		//iterate the modules
		for (int i = 0; i < modNames.size(); i++) {
			try {
				//grab a mod class
				String modName = (String) modNames.get(i);
				Class modclass = Class.forName(modName);
				java.lang.reflect.Constructor constructor;
				Class[] carr = {this.getClass()};
				Object[] oarr = {this};
				//make a constructor
				constructor = modclass.getConstructor(carr);
				
				//get an instance
				BotModule mod = (BotModule) constructor.newInstance(oarr);
             
				//register the mod
				registerModule(mod);
				
				logger.info("Loading mod (" + mod.getName() + ")");
			} catch (Exception e) {
				e.printStackTrace();
				logger.severe("Unable to load mod=" + modNames.get(i));
			}
		}
	}


	/**
	 * Method to persist all state to XML. Saves username, password, all
	 * buddies, all groups. Modules and services will be reloaded on
	 * depersistance so we dont need to save them; just give them a chance to
	 * persist and then depersist them next time loadmoduels is called.
	 * 
	 * @return returns true iff the persistance succeeded.
	 */
	  
	private boolean expPersist() 
	{       
	        
	 		for (Iterator iter = aim.getBuddyNames(); iter.hasNext();) 
			{
    			String name = (String) iter.next();
    			AIMBuddy bud = aim.getBuddy(name);
    			if (bud == null) 
    			{
    				continue;
    			}
			
			   bud.expWriteState(this.im_id,name,2,2);
			
		}
	     return true;
	} 
	
	
	
	/**
	 * Method expsync
	 * 
	 * @return .
	 */ 
	 
	public int expsync() 
	{
	   String name_str="";
	   String sql_str="";
	   
        for (Iterator iter = aim.getBuddyNames(); iter.hasNext();) 
    	{
            String name = (String) iter.next();
            AIMBuddy bud = aim.getBuddy(name);
            if (bud == null) 
            {
            	continue;
            }
			if(name_str.length() > 0)
			{
			   name_str = name_str + ",'"+name+"'";
			}
			else if(name_str.length() < 1)
			{
			   name_str =  "'"+name+"'";
			}
			
			// insert into db
			//bud.expWriteState(this.im_id,name,2,2);
			 //logger.info("adding buddy -- "+name);
        }
		
		if(name_str.length() < 1)
		{
		  name_str = "dummy";
		}
		
		//add buddies
	    sql_str = "select A.screen_name,A.banned,B.name,C.name "+
		           "from im_buddies as A,"+
				        "im_groups as B,"+
						"im_roles as C "+
				   "where A.im_id = "+ this.im_id +
				   " and  A.group_id = B.group_id "+
				   " and  A.role_id  = C.role_id "+
				   " and A.active = 'Y' "+
				   " and A.screen_name not in (" +name_str +")";
						
        this.test.RunSql(sql_str);
		if(this.test.totalNumRows >=1)
	    {
    		try 
            {
        	      while (this.test.rs.next())
            	  {
    			      String name    = this.test.rs.getString(1);
    				  String group   = this.test.rs.getString(3);
    				  String role    = this.test.rs.getString(4);
    				  String banned  = this.test.rs.getString(2);
        			  AIMBuddy buddy = new AIMBuddy(name);
        			  buddy.expReadState(name,group,role);
        			  aim.addBuddy(buddy);
            	   
            	  }
    	  	}
    		catch (Exception e) 
            {   
    		              
               logger.info("Error in Aim Bot -- "+sql_str);
            }
    	}	
		this.test.CloseResultSet();
		
		
		//add counselors
	    sql_str = "select A.screen_name "+
		           "from im_counselors as A"+
				   " where A.im_id = "+ this.im_id +
				   " and A.active = 'Y' " +
				   " and A.screen_name not in (" +name_str +")";
						
        this.test.RunSql(sql_str);
		if(this.test.totalNumRows >=1)
	    {
    		try 
            {
        	      while (this.test.rs.next())
            	  {
    			      String name    = this.test.rs.getString(1);
    				  String group   = "Recent Buddies";
    				  String role    = "Counselor";
    				  String banned  = "N";
        			  AIMBuddy buddy = new AIMBuddy(name);
        			  buddy.expReadState(name,group,role);
        			  aim.addBuddy(buddy);
            	   
            	  }
    	  	}
    		catch (Exception e) 
            {   
    		              
               logger.info("Error in Aim Bot -- "+sql_str);
            }
		}
		this.test.CloseResultSet();
		
		return 1;
		
	} 
	/**
	 * Method depersist.
	 * 
	 * @return returns true iff the depersistance succeeded.
	 */
	private boolean expDePersist() 
	{
	
	    String sql_str="";
		/*
		for (Iterator iter = aim.getBuddyNames(); iter.hasNext();) 
		{
		    // logger.info(" already existing  -- "+iter.next());
	    }
		*/
		//add buddies
	    sql_str = "select A.screen_name,A.banned,B.name,C.name "+
		           "from im_buddies as A,"+
				        "im_groups as B,"+
						"im_roles as C "+
				   "where A.im_id = "+ this.im_id +
				   " and  A.group_id = B.group_id "+
				   " and  A.role_id  = C.role_id "+
				   " and A.active = 'Y' ";
						
        this.test.RunSql(sql_str);
		if(this.test.totalNumRows >=1)
	    {
    		try 
            {
        	      while (this.test.rs.next())
            	  {
    			      String name    = this.test.rs.getString(1);
    				  String group   = this.test.rs.getString(3);
    				  String role    = this.test.rs.getString(4);
    				  String banned  = this.test.rs.getString(2);
        			  AIMBuddy buddy = new AIMBuddy(name);
        			  buddy.expReadState(name,group,role);
        			  aim.addBuddy(buddy);
            	   
            	  }
    	  	}
    		catch (Exception e) 
            {   
    		              
               logger.info("Error in Aim Bot -- "+sql_str);
            }
    	}	
		this.test.CloseResultSet();
		
		
		//add counselors
	    sql_str = "select A.screen_name "+
		           "from im_counselors as A"+
				   " where A.im_id = "+ this.im_id +
				   " and A.active = 'Y' ";
						
        this.test.RunSql(sql_str);
		if(this.test.totalNumRows >=1)
	    {
    		try 
            {
        	      while (this.test.rs.next())
            	  {
    			      String name    = this.test.rs.getString(1);
    				  String group   = "Recent Buddies";
    				  String role    = "Counselor";
    				  String banned  = "N";
        			  AIMBuddy buddy = new AIMBuddy(name);
        			  buddy.expReadState(name,group,role);
        			  aim.addBuddy(buddy);
					   logger.info("adding counselor  -- "+name);
            	   
            	  }
    	  	}
    		catch (Exception e) 
            {   
    		              
               logger.info("Error in Aim Bot -- "+sql_str);
            }
		}
		this.test.CloseResultSet();
		
		
	
	 	
	     
	    return true;
	
	} 
	
	/**
	 * createDomDocument - creates an empty document
	 * 
	 * @return Document - returns an empty document
	 */
	protected static Document createDomDocument() {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.newDocument();
			return doc;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		return null;
	}


	/**
	 * Registers a specific module to handle requests that start with a specific
	 * string
	 * 
	 * @param keyword
	 *            a word to look for in client text that identifies a bot module
	 *            to be called
	 * @param mod
	 *            the bot module that will handle this request
	 */
	private void registerService(String keyword, BotModule mod) {
		services.put(keyword, mod);
	}
	
	/* Following are the Express functions */
	
	// get screen name 
	public String getScreenName() 
	{
	   
        return this.username;
    }
	
	// set  IM ID
	public void setImId(Integer im_id) 
	{
         this.im_id = im_id;
    }
		
	// get  IM ID
	public Integer getImId() 
	{   
        return this.im_id;
    }
	
	
	// get  IM name
	public String getImName() 
	{   
        return this.im_name;
    }
	
	// get  IM start time
	public String getStartTime() 
	{   
        return this.startTime;
    }
	
	
	// get  IM default message
	public String getDefaultMessage() 
	{   
        return this.defMessage;
    }
	
	// get  IM start time
	public Integer getTimeDelay() 
	{   
        return this.time_delay;
    }
	// Make the initial IM assignments
	public void getImDetails()
	{
	   String sql_str="";
	   sql_str =  "select  A.name,A.im_id FROM im_client_bots as A where A.screen_name = '" 
	           + this.username + "' ";	   
	    this.test.RunSql(sql_str);
		
		try 
        {
    	      this.test.rs.next();
              this.im_name        = this.test.rs.getString(1);
			  this.setImId(this.test.rs.getInt(2));
	  	}
		catch (Exception e) 
        {   
                      
        }
	
	
	}
	
	
	
	}