/*------------------------------------------------------------------------------
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is levelonelabs.com code.
 * The Initial Developer of the Original Code is Level One Labs. Portions
 * created by the Initial Developer are Copyright (C) 2001 the Initial
 * Developer. All Rights Reserved.
 *
 *         Contributor(s):
 *             Scott Oster      (ostersc@alum.rpi.edu)
 *             Steve Zingelwicz (sez@po.cwru.edu)
 *             William Gorman   (willgorman@hotmail.com)
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable
 * instead of those above. If you wish to allow use of your version of this
 * file only under the terms of either the GPL or the LGPL, and not to allow
 * others to use your version of this file under the terms of the NPL, indicate
 * your decision by deleting the provisions above and replace them with the
 * notice and other provisions required by the GPL or the LGPL. If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under the terms of any one of the NPL, the GPL or the LGPL.
 *----------------------------------------------------------------------------*/

package com.levelonelabs.aim;

import org.w3c.dom.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.lang.Integer;
import java.lang.Number;
import java.lang.Object;
import com.targetx.ExpressDbMgr;
import java.util.logging.Logger;

/**
 * Represents and AIM Buddy
 * 
 * @author Will Gorman, Scott Oster
 * @created November 8, 2001
 */
public class AIMBuddy implements XMLizable {
    String name;
    transient boolean online;
    transient int warningAmount = 0;
    boolean banned;
    ArrayList messages = new ArrayList();
	ArrayList messageIdArr = new ArrayList();
    HashMap roles = new HashMap();
    HashMap preferences = new HashMap();
    String group;
    public String Name_module=""; // to remember module name 
	public String Name_sub_module; // to remember sub module name 
    public String Proxy_buddy; // to remember proxy buddy 
	public String Proxy_initial="no"; // to remember proxy buddy 
	public String Last_message; // The last message user typed
	public String Name_counselor; // to remember counselor name 
	public int    Is_counselor=0;//check whether the buddy is a counselor
	public int    Counselor_group_id=0;// Counselor group id
	public int    Is_idle=0;//check whether the buddy is idle
	public ExpressDbMgr test = new ExpressDbMgr();
	static Logger logger=Logger.getLogger("AIMBuddy");
    /**
     * Constructor for the AIMBuddy object
     * 
     * @param name
     */
    public AIMBuddy(String name) {
        this(name, AIMClient.DEFAULT_GROUP);
    }


    /**
     * Constructor for the AIMBuddy object
     * 
     * @param name
     * @param group
     *            buddy group in buddylist
     */
    public AIMBuddy(String name, String group) {
        this.name = name;
        setGroup(group);
    }


    /**
     * Sets the name attribute of the AIMBuddy object
     * 
     * @param name
     *            The new name value
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Sets the online attribute of the AIMBuddy object
     * 
     * @param online
     *            The new online value
     */
    public void setOnline(boolean online) {
        this.online = online;
    }

    /**
     * Sets the preference attribute of the AIMBuddy object
     * 
     * @param pref
     *            The new preference value
     * @param val
     *            The new preference value
     */
    public void setPreference(String pref, String val) {
        preferences.put(pref, val);
    }


    /**
     * Gets the name attribute of the AIMBuddy object
     * 
     * @return The name value
     */
    public String getName() {
        return name;
    }


    /**
     * Gets the online attribute of the AIMBuddy object
     * 
     * @return The online value
     */
    public boolean isOnline() {
        return online;
    }


    /**
     * Gets the preference attribute of the AIMBuddy object
     * 
     * @param pref
     * @return The preference value
     */
    public String getPreference(String pref) {
        return (String) preferences.get(pref);
    }


    /**
     * Gets the preferences attribute of the AIMBuddy object
     * 
     * @return The preferences value
     */
    public HashMap getPreferences() {
        return preferences;
    }


    /**
     * Adds a feature to the Role attribute of the AIMBuddy object
     * 
     * @param role
     *            The feature to be added to the Role attribute
     */
    public void addRole(String role) {
        roles.put(role, role);
    }


    /**
     * Gets the messages attribute of the AIMBuddy object
     * 
     * @return The messages value
     */
    public ArrayList getMessages() {
        return messages;
    }
    
	 public String expGetMessages(AIMBuddy to,int  imid,int message_id) 
	 {
        String sql_str="";
		String message_str="";
		String status =""; 
		int actual_message_id=0;
		
		
		if( message_id < 1)
		{
		     message_str = "<table border='0'><tr><td>seq.# </td><td>from        </td><td>  Left</td>"+"</tr>";      
	         sql_str = "select serial,from_screen_name,mod_time,status  from im_messages where im_id = "+
		             imid + " and to_screen_name = '"+to.getName()+"' and status in('R','Y')";
	    }
		
		if( message_id > 0)
		{
		    actual_message_id = Integer.parseInt(messageIdArr.get(message_id-1).toString());
		     message_str = "";      
	        sql_str = "select message as mess from im_messages where im_id = "+
		             imid + " and to_screen_name = '"+to.getName()+"' and status in('Y','R') and serial = "+actual_message_id ;
	    }
		  
		this.test.RunSql(sql_str);
		
		if(this.test.totalNumRows < 1)
	    {
	      return  "";
	    }
		int i=0;
		int j=1;
		
		try 
        {      
		     
    		 if( message_id < 1)
    		 {        
			         // Clear the arry message id arry if the messages are retrieving again
					 messageIdArr.clear();
        		     while( test.rs.next())
        			 {  status =  test.rs.getString(4); 
					    //System.out.println("trace 2.."+j);
					    if(status.startsWith("R")) 
						{
						  
            	          message_str +=  "<tr><td>"+j+"</td>"+ "<td>"+test.rs.getString(2)+"</td>"+"<td>"+test.rs.getString(3)+"</td>"+"</tr>";
						}
						else if(status.startsWith("Y")) 
						{
						  
						  message_str +=  "<tr><td><b>"+j+"</b></td>"+ "<td><b>"+test.rs.getString(2)+"</b></td>"+"<td><b>"+test.rs.getString(3)+"</b></td>"+"</b></tr>";
						}
						
						messageIdArr.add(i,test.rs.getInt(1));
						//System.out.println("Array  -- "+messageIdArr.get(i));
						i++;
						j++;
						
        			 }
    		  }
    		  else if ( message_id > 0)
    		  {
    		        test.rs.next();
    		        message_str  = test.rs.getString(1);
    		  }
		  
		  
	  	}
		catch (Exception e) 
        {   
                     logger.info("Error in buddy module -- "+sql_str);
					 e.printStackTrace();
        }
		
		
		// This is to mark the message is red.
		if( message_id > 0)
		{
		 	 sql_str = "update im_messages  set status = 'R' where im_id = "+
    		             imid + " and to_screen_name = '"+to.getName()+"' and status = 'Y' and serial = "+actual_message_id ;
			this.test.RunSql(sql_str);			 
		
		}
		
		
		message_str += "</table><BR>Type <b>Read #</b> to read the message"+
		              " <BR> Type<b> Delete # </b> to delete the message <BR>"+
					  "Type  <b>Clear Messages</b> to Clear all messages<br> Type <b>Quit</b> to quit the Inbox";
		return  message_str;
    }

    /**
     * Adds a feature to the Message attribute of the AIMBuddy object
     * 
     * @param message
     *            The feature to be added to the Message attribute
     */
    public void addMessage(String message) {
        //messages.add(message);
		
    }
	// add messages
	public void expAddMessage(AIMBuddy to, AIMBuddy from,int  imid,String message) 
	{
	    String sql_str="";
	    sql_str = "insert into im_messages(im_id,from_screen_name,to_screen_name,message)values("+
		             imid + ",'"+from.getName()+"','"+to.getName()+"','"+message+"')";
	      
		   //System.out.println(" AIMBuddy module-- "+sql_str);
		try 
        {     
    	    this.test.RunSql(sql_str);
	  	}
		catch (Exception e) 
        {   
            //  System.out.println("Error in AIMBuddy module -- "+sql_str);
        }
 
    } 
	// log message
	public void expLogMessage(AIMBuddy to, AIMBuddy from,int  imid,String message) 
	{
	    String sql_str="";
	    sql_str = "insert into im_msg_log(im_id,from_screen_name,to_screen_name,message)values("+
		             imid + ",'"+from.getName()+"','"+to.getName()+"','"+message+"')";
	      
		   //System.out.println(" AIMBuddy module-- "+sql_str);
		try 
        {     
    	    this.test.RunSql(sql_str);
	  	}
		catch (Exception e) 
        {   
            //  System.out.println("Error in AIMBuddy module -- "+sql_str);
        }
 
    } 
	
	
    /**
     * Remove all messages
     */
    public void clearMessages() {
        messages.clear();
    }
	  
	 public void expClearMessages(AIMBuddy to,int imid,int message_id) 
	 {
	 
    	  	String sql_str="";
    		String message_str="";
			Integer actual_message_id=0;
			
    		
    		if(message_id < 1)
    		{
    		     
    	         sql_str = "update im_messages  set status = 'D' where im_id = "+
    		             imid + " and to_screen_name = '"+to.getName()+"' ";
    	    }
    		
    		if(message_id > 0)
    		{
			
			

			    actual_message_id = Integer.parseInt(messageIdArr.get(message_id-1).toString());
    	        sql_str = "update im_messages  set status = 'D' where im_id = "+
    		             imid + " and to_screen_name = '"+to.getName()+"' and  serial = "+actual_message_id ;
    	    }
    		  
    		
    		try 
            {    
    		      this.test.RunSql(sql_str);
    	  	}
    		catch (Exception e) 
            {   
                          //System.out.println(" AIMBuddy delete-- "+sql_str);
            }
		
       
     }


    /**
     * Does buddy have messages?
     * 
     * @return true for more than 0 messages
     */
	  public boolean hasMessages() {
        return !messages.isEmpty();
    }
	
    public int expHasMessages(AIMBuddy buddy,int  imid) 
	{
        String sql_str="";
		Integer total_messages=0;
		
	        sql_str = "select count(*) as mess_count from im_messages where im_id = "+
		             imid + " and to_screen_name = '"+buddy.getName()+"' and status in ('R','Y') " ;
	  
		  
		this.test.RunSql(sql_str);
		
		if(this.test.totalNumRows < 1)
	    {
	      return  0;
	    }
		try 
        {     test.rs.next();
		      total_messages = test.rs.getInt(1);
		       
    	     
	  	}
		catch (Exception e) 
        {   
                      // logger.info("Error in AIMBuddy module -- "+sql_str);
        }
		
		if(total_messages > 0)
		{
		  return total_messages;
		}
		
		 return 0;
    }
	
	


    /**
     * Do I have specified role
     * 
     * @param role
     * @return true if buddy has the role
     */
    public boolean hasRole(String role) {
        return roles.containsKey(role);
    }


    /**
     * Returns the banned.
     * 
     * @return boolean
     */
    public boolean isBanned() {
        return banned;
    }


    /**
     * Sets the banned.
     * 
     * @param banned
     *            The banned to set
     */
    public void setBanned(boolean banned) {
        this.banned = banned;
    }


    /**
     * @see com.levelonelabs.aim.XMLizable#readState(Element)
     */
	 public void expReadState(String name,String group,String role) 
	 {
	  
	      // set group
    	  if (group == null || group.trim().equals("")) 
    	  {
                group = AIMSender.DEFAULT_GROUP;
          }
          setGroup(group);
	      // set banned = false
	      setBanned(false);
		  
		  // set role
		  addRole(role);
		  
		  
	  
	 }
	 
	  public void addBuddiesCounselor(AIMBuddy buddy,int counselor_id,int im_id,int counselor_group_id) 
	 {
	    int buddies_id=0;
		String sql_str="";
	    // Get buddie's id
		  sql_str = "select buddies_id  from im_buddies where im_id = "+
		             im_id + " and screen_name = '"+buddy.getName()+"' " ;
	  
		  
		this.test.RunSql(sql_str);
		
		
		if(this.test.totalNumRows >= 1)
	    {
	      
	   
    		//System.out.println("Here  -- "+sql_str);
    		try 
            {     test.rs.next();
    		      buddies_id = test.rs.getInt(1);    
        	     
    	  	}
    		catch (Exception e) 
            {   
                          // logger.info("Error in AIMBuddy module -- "+sql_str);
            }   
		}
		
		sql_str = "insert ignore into im_buddies_counselor(im_id,buddies_id,counselor_id,counselor_group_id)values("+
		           im_id +","+buddies_id+","+counselor_id+","+counselor_group_id +")";
		  
		  
		this.test.RunSql(sql_str);  
		
		//System.out.println("Here 2 -- "+sql_str);
	  
	 }
	 
	 public void readState(Element fullStateElement) 
	 {}
	  public void writeState(Element emptyStateElement) 
	  {}
/*	 
    public void readState(Element fullStateElement) {
        // parse group
        String group = fullStateElement.getAttribute("group");
        if (group == null || group.trim().equals("")) {
            group = AIMSender.DEFAULT_GROUP;
        }
        setGroup(group);
		  System.out.println(" read state -- "+fullStateElement+"---"+group);

        // parse banned
        String ban = fullStateElement.getAttribute("isBanned");
        if (ban.equalsIgnoreCase("true")) {
            setBanned(true);
        } else {
            setBanned(false);
        }

        // parse roles
        roles = new HashMap();
        NodeList list = fullStateElement.getElementsByTagName("role");
        for (int i = 0; i < list.getLength(); i++) {
            Element roleElem = (Element) list.item(i);
            String role = roleElem.getAttribute("name");
            addRole(role);
        }

        // parse messages
        messages = new ArrayList();
        list = fullStateElement.getElementsByTagName("message");
        for (int i = 0; i < list.getLength(); i++) {
            Element messElem = (Element) list.item(i);
            NodeList cdatas = messElem.getChildNodes();
            for (int j = 0; j < cdatas.getLength(); j++) {
                Node node = cdatas.item(j);
                if (node.getNodeType() == Node.CDATA_SECTION_NODE) {
                    String message = node.getNodeValue();
                    addMessage(message);
                    break;
                }
            }
        }

        // parse prefs
        preferences = new HashMap();
        list = fullStateElement.getElementsByTagName("preference");
        for (int i = 0; i < list.getLength(); i++) {
            Element prefElem = (Element) list.item(i);
            String pref = prefElem.getAttribute("name");
            String val = prefElem.getAttribute("value");
            this.setPreference(pref, val);
        }
    }
*/

    /**
     * @see com.levelonelabs.aim.XMLizable#writeState(Element)
     */
	  public void expWriteState(Integer imid,String name,Integer group,Integer role) 
	  {
	      String sql_str="";
		  Integer buddy_count=0;
		  Integer counselor_count=0;
	      sql_str = "select screen_name  from im_buddies where im_id = "+
		             imid + " and screen_name = '"+name+"' and active = 'Y' " ;
    		this.test.RunSql(sql_str);
			buddy_count = this.test.totalNumRows;
			//System.out.println("first sql-- "+sql_str);
			sql_str = "select screen_name from im_counselors where im_id = "+
		             imid + " and screen_name = '"+name+"' and active = 'Y' " ;
    		this.test.RunSql(sql_str);
			counselor_count = this.test.totalNumRows;
			//System.out.println("second sql-- "+sql_str);
			//System.out.println("adding buddy -- "+buddy_count +"----"+counselor_count);
    		if(buddy_count < 1 & counselor_count < 1 )
    		{
    		
    		        sql_str = "insert into im_buddies(im_id,screen_name,group_id,role_id)values("+
    		                imid + ",'"+name+"',2,2)" ;
    			    
            		try 
                    {     
            		      this.test.RunSql(sql_str);
            	  	}
            		catch (Exception e) 
                    {   
                                  // logger.info("Error in AIMBuddy module -- "+sql_str);
                    }
    		}
	  }
	  /*
      public void writeState(Element emptyStateElement) 
	  {
	    
		
        Document doc = emptyStateElement.getOwnerDocument();
        emptyStateElement.setAttribute("name", this.getName());
        emptyStateElement.setAttribute("group", this.getGroup());
        emptyStateElement.setAttribute("isBanned", Boolean.toString(this.isBanned()));

		 //System.out.print("depersist-1- "+this.getName());
		 
		   
        Iterator roleit = roles.keySet().iterator();
        while (roleit.hasNext()) {
            String role = (String) roleit.next();
            Element roleElem = doc.createElement("role");
            roleElem.setAttribute("name", role);
            emptyStateElement.appendChild(roleElem);
        }

        Iterator prefs = preferences.keySet().iterator();
        while (prefs.hasNext()) {
            String pref = (String) prefs.next();
            Element prefElem = doc.createElement("preference");
            prefElem.setAttribute("name", pref);
            prefElem.setAttribute("value", (String) preferences.get(pref));
            emptyStateElement.appendChild(prefElem);
        }

        for (int i = 0; i < messages.size(); i++) {
            String message = (String) messages.get(i);
            Element messElem = doc.createElement("message");
            CDATASection data = doc.createCDATASection(message);
            messElem.appendChild(data);
            emptyStateElement.appendChild(messElem);
        }
    }
*/

    /**
     * Gets the current warning amount
     * 
     * @return the warning amount
     */
    public int getWarningAmount() {
        return warningAmount;
    }


    /**
     * Sets the current warning amount
     * 
     * @param amount
     */
    public void setWarningAmount(int amount) {
        warningAmount = amount;
    }


    /**
     * Set the group the buddy is in, in the buddy list
     * 
     * @param group
     *            The name of the group this buddy belongs to.
     */
    public void setGroup(String group) {
        this.group = group;
    }

 
    /**
     * The name of the group this buddy belongs to, in the buddylist
     * 
     * @return The name of the group this buddy belongs to.
     */
    public String getGroup() {
        return group;
    }
	
	// get module name 
	public String getModule() 
	{
	   //System.out.print("Here10-- ");
        return this.Name_module;
    }
	
	// set  module name 
	public void setModule(String Module_name) 
	{
	    //System.out.print("Here11-- "+Module_name);
         this.Name_module = Module_name;
    }
	 
	// Get proxy buddy name
	public String getProxyBuddy() 
	{
	    
        return this.Proxy_buddy;
    }
	
	// Set  Proxy  
	public void setProxyBuddy(String buddy) 
	{
	    //System.out.print("Here13-- "+buddy);
         this.Proxy_buddy = buddy;
    }
	
	// get Proxy_initial 
	public String getProxyInitial() 
	{
	   
        return this.Proxy_initial;
    }
	
	// set Proxy_initial name 
	public void setProxiInitial(String proxy_stage) 
	{
	   
         this.Proxy_initial = proxy_stage;
	}
	
	// get Last_message 
	public String getLastMessage() 
	{
	   
        return this.Last_message;
    }
	
	// set  Last_message 
	public void setLastMessage(String message) 
	{
	    
         this.Last_message = message;
	}
	
	
	// get sub module name 
	public String getSubModule() 
	{
	  
        return this.Name_sub_module;
    }
	
	// set sub  module name 
	public void setSubModule(String Name_sub_module) 
	{
	   
         this.Name_sub_module = Name_sub_module;
    }
	
	   
	// get Counselor name 
	public String getCounselor() 
	{
	   
        return this.Name_counselor;
    }
	
	// set Counselor name  
	public void setCounselor(String Name_counselor) 
	{
	   
         this.Name_counselor = Name_counselor;
    }
	
	// get Is Counselor  
	public int getIsCounselor() 
	{
	   
        return this.Is_counselor;
    }
	
	// set IS Counselor 
	public void setIsCounselor(Integer Is_counselor) 
	{
	    
         this.Is_counselor = Is_counselor;
    }
	
	// get counselor group id 
	public int getCounselorGroupId() 
	{
	   
        return this.Counselor_group_id;
    }
	
	// set counselor group id
	public void setCounselorGroupId(Integer Counselor_group_id) 
	{
	    
         this.Counselor_group_id = Counselor_group_id;
    }
	
	// set counselor group id
	public void setCounselorGroupId_(Integer Counselor_group_id) 
	{
	    
         this.Counselor_group_id = Counselor_group_id;
    }
	
	
	// get is idle
	public int getIsIdle() 
	{
	    return this.Is_idle;
         
    }
	
	// set is idle
	public void setIsIdle(Integer Is_idle) 
	{
	   this.Is_idle = Is_idle;
        
    }
	
	
	
}
