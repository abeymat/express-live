package com.targetx.modules;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.levelonelabs.aim.AIMBuddy;
import com.levelonelabs.aimbot.AIMBot;
import com.levelonelabs.aimbot.BotModule;
       
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Timer;
import java.util.TimerTask;
/*
 * Handles requests to get Targetx info
 */
 
public class TargetxProxyModule extends BotModule 
{
    static Logger logger=Logger.getLogger(TargetxProxyModule.class.getName());
    private static ArrayList services;
	public  int step=0;
	public CounselorResponseCheck watchdogCheck;
	public ProxyModeCheck watchdogProxy;
    boolean cResponseVerified = false;
    public Timer counselorCheck = new Timer();
	public Timer proxyCheck = new Timer();
    
	public String counselor;
    /**
     * Initialize the service commands.
     */
    static 
	{ 
        services=new ArrayList();        
		services.add("proxy");	
    } 
	
	 /**
     * Constructor for TargetxModule.
     *
     * @param bot
     */
    public TargetxProxyModule(AIMBot bot) {
        super(bot);
    }

    /**
     * @see com.levelonelabs.aimbot.BotModule#getName()
     */
    public String getName() {
        return "Targetx Proxy  Module";
    }

	
	
    /**
     * @see com.levelonelabs.aimbot.BotModule#help()
     */
    public String help() {
        StringBuffer sb=new StringBuffer();
        sb.append(
            "<B>Climate <i>ZIPCODE</i></B> (displays the 5 day forcast for the specified zipcode)\n");
        sb.append(
            "* If the preference \"zipcode\" is set, you can omit the zipcode to use your default.");
        return sb.toString();
    }


    /**
     * @see com.levelonelabs.aimbot.BotModule#getServices()
     */
    public ArrayList getServices() {
        return services;
    }
	
	 /**
     * Return the list name
     *
     * @param buddy the buddy
     * @param query the list id
     */
    public void performService(AIMBuddy buddy, String query) 
	{
	    String module="";
		String def_coun_message="";
		String dis_message;
		String sql_str="";
		int TIME_DELAY = this.bot.getTimeDelay();
		logger.info("In " + getName());
		
		//this.counselorCheck.cancel();
        if(query.trim().toLowerCase().startsWith("proxy"))
		{  
		    StringTokenizer st = new StringTokenizer(query, " ");
			String command =  st.nextToken();
		    this.counselor =  st.nextToken();
			this.step      = 0;
	 
			buddy.setProxyBuddy(this.counselor);
			
		    AIMBuddy to = getBuddy(this.counselor);
    		//sendMessage(buddy, "hey it is working");
			
            super.sendMessage(to, " Would you like to talk to the student " +buddy.getName()+ "<br> please type <b>yes</b> or <b>no</b>" );
			sendMessage(buddy, "Please wait while I see if a counselor is currently available");
			//set the counselors module to proxy
			
			to.setModule("proxy");
			//buddy.setModule("proxy");
			buddy.setProxiInitial("yes");
			
			to.setProxiInitial("yes");
			//set the counselors module buddy to 
			to.setProxyBuddy(buddy.getName());		
			
			 // check the response of the counselor		 
             watchdogCheck = new CounselorResponseCheck(this,buddy,to,query);
			 try
			 {
                this.counselorCheck.schedule(watchdogCheck,TIME_DELAY);
			 }
			 catch (Exception e) 
             {              
                  e.printStackTrace();
             }
		}
		else if( query.trim().toLowerCase().startsWith("no") && buddy.getProxyInitial().equals("yes") )
		{
    		    buddy.setModule("start");
    			
    			AIMBuddy to = getBuddy(buddy.getProxyBuddy());
    			
    			buddy.setProxiInitial("no");
    			to.setProxiInitial("no");
    			
    			//set the counselors module to counselor module
    			// to.setModule("talk"); -- commented on feb 12
    			// bot.handleMessage(buddy,query,module);
    			buddy.setProxyBuddy("");
    			
    			sql_str = 	"select  A.def_message FROM im_counselors as A where " +
    			           " A.im_id = " + this.bot.getImId()   +
                    " and A.screen_name ='" + this.counselor.toLowerCase() +"'";   
    			   	   
        	    this.bot.test.RunSql(sql_str);
        		if(this.bot.test.totalNumRows >=1)
    	        {
            		try 
                    {
                	      this.bot.test.rs.next();
                        
        				  def_coun_message = this.bot.test.rs.getString(1);
            	  	}
            		catch (Exception e) 
                    {   
                         //System.out.println("3---");        
                    }
    		    }
    			if(def_coun_message.length() > 1)
    			{
    			  dis_message = def_coun_message ;
    			
    			}
    			else
    			{
        			 dis_message = "I'm sorry but a counselor is not currently available. <br>Please leave a message for your counselor  ";
                    
    			}
    			
    		    super.sendMessage(to,dis_message );
    			
    			/* if( buddy.getCounselor().length()> 0)
    			   {
    			     counselor_scr_nm = buddy.getCounselor();
    				 
    			   }
    		      */
    		        
    		        module="tell";
        			query = "counselor " + this.counselor + " "  ;
        	        bot.handleMessage(to,query,module);
		}
		else if( query.trim().toLowerCase().startsWith("yes") && buddy.getProxyInitial().equals("yes") )
		{
		   AIMBuddy to = getBuddy(buddy.getProxyBuddy());
		   super.sendMessage(buddy,"You can talk now. You may end your conversation anytime by typing <b>Exit</b>");
		   //super.sendMessage(to, "Your counselor," + this.counselor +", is available. Go ahead and type your message.  You may end your conversation anytime by typing <b>Exit</b>");
		   super.sendMessage(to,"You can talk to a counselor now. Go ahead and type your message. You may end your conversation anytime by typing  <b>Exit</b>");

		   this.step = 1;
		   buddy.setProxiInitial("no");
		   to.setProxiInitial("no");
		   to.setIsIdle(1);
		   // check the response of the counselor		 
           watchdogProxy = new ProxyModeCheck(this,buddy,to,query);
		   try
		   {
                this.proxyCheck.schedule(watchdogProxy,TIME_DELAY,TIME_DELAY);
		   }
			 catch (Exception e) 
             {              
                  e.printStackTrace();
             }
		}
		else if( query.trim().toLowerCase().equals("exit"))
		{
		   AIMBuddy to = getBuddy(buddy.getProxyBuddy());
		   
		   String msg = "You are no longer communicating with the other person";
		   super.sendMessage(to,msg);
		   this.step = 0;
		   super.sendMessage(buddy,msg);
		   buddy.setProxyBuddy("");
		   to.setProxyBuddy("");
		   buddy.setProxiInitial("");
		   to.setProxiInitial("");
		   buddy.setModule("start");
		   to.setModule("start");
		   bot.handleMessage(buddy,"start","start");
		   bot.handleMessage(to,"start","start");
		   this.proxyCheck.cancel();
		       
		}
		else if(this.step == 1)
		{   
		    AIMBuddy to = getBuddy(buddy.getProxyBuddy());
			
		     buddy.setProxiInitial("no");
			 to.setProxiInitial("no");
			               
			 super.sendMessage(to,query  );
			 // log the conversation
			 buddy.expLogMessage(to,buddy,this.bot.getImId(),query);
			 // set idle value to "0"
		     to.setIsIdle(0);
		
		}
		
    } 
	
	
	   public void performService1(AIMBuddy counselor,AIMBuddy buddy, String query) 
	   {
           super.sendMessage(counselor,query);
		   buddy.setProxyBuddy("");
		   counselor.setProxyBuddy("");
		   buddy.setProxiInitial("");
		   counselor.setProxiInitial("");
		   //buddy.setModule("start");
		   //counselor.setModule("start");
	   }
	   
	   
	   
	  public static void main(String[] args) 
	  {
            TargetxProxyModule mod= new TargetxProxyModule(null);
            String list_id = args[0];
            String result="Couldn't get weather information";
            if(list_id.length() == 8) 
    		{
                result="Couldn't get weather information";
            }
            //System.out.println(result);
	   }
	   
	  /****************Class CounselorResponseCheck begin****************/
	  static class CounselorResponseCheck extends TimerTask 
	  { 
	    String response;
		AIMBuddy counselor; 
		AIMBuddy buddy; 
		TargetxProxyModule proxy;
        public CounselorResponseCheck(TargetxProxyModule proxy,AIMBuddy buddy,AIMBuddy counselor, String request) 
		{
           this.response = request;
		   this.counselor    = counselor;
		   this.proxy     = proxy;
		   this.buddy     = buddy;
		    //proxy.sendMessage(this.buddy, "Sorry counselor not available");
		   System.out.println("I am Here 1 -- "+buddy.getName()+"--"+counselor.getName()+"---"+request);
        }
        
        public void run() 
		{
		    //System.out.println("I am Here 2 -- ");
            try 
			{
			   if( this.counselor.getProxyInitial().equals("yes")&& this.buddy.getProxyInitial().equals("yes"))
			   {
			   
			      this.proxy.performService(this.counselor, "No");
				  this.proxy.performService1(this.counselor,this.buddy,"Request cancelled");
			   }
			           
            } 
			catch (Exception e) 
			{
                e.printStackTrace();
            }
        }
    }
	
	/****************Class CounselorResponseCheck end************/
	/****************Class ProxyModeCheck begin****************/
	  static class ProxyModeCheck extends TimerTask 
	  { 
	    String response;
		AIMBuddy counselor; 
		AIMBuddy buddy; 
		TargetxProxyModule proxy;
        public ProxyModeCheck(TargetxProxyModule proxy,AIMBuddy buddy,AIMBuddy counselor, String request) 
		{
           this.response = request;
		   this.counselor    = counselor;
		   this.proxy     = proxy;
		   this.buddy     = buddy;
		    //proxy.sendMessage(this.buddy, "Sorry counselor not available");
		   System.out.println("I am Here 1 -- "+buddy.getName()+"--"+counselor.getName()+"---"+request);
        }
        
        public void run() 
		{
		    //System.out.println("I am Here 2 -- ");
            try 
			{
			   if( this.counselor.getIsIdle() == 1)
			   {
			   
			      this.proxy.performService(this.counselor, "exit");
				  //this.proxy.performService1(this.counselor,this.buddy,"Request cancelled");
			   }
			   else
			   {
			     this.counselor.setIsIdle(1);
			   
			   }
			           
            } 
			catch (Exception e) 
			{
                e.printStackTrace();
            }
        }
    }
	
	/****************Class ProxymodeCheck end************/

}
