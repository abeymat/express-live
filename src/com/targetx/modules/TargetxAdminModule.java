/*------------------------------------------------------------------------------
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is levelonelabs.com code.
 * The Initial Developer of the Original Code is Level One Labs. Portions
 * created by the Initial Developer are Copyright (C) 2001 the Initial
 * Developer. All Rights Reserved.
 * 
 *         Contributor(s):
 *             Scott Oster      (ostersc@alum.rpi.edu)
 *             Steve Zingelwicz (sez@po.cwru.edu)
 *             William Gorman   (willgorman@hotmail.com)
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable
 * instead of those above. If you wish to allow use of your version of this
 * file only under the terms of either the GPL or the LGPL, and not to allow
 * others to use your version of this file under the terms of the NPL, indicate
 * your decision by deleting the provisions above and replace them with the
 * notice and other provisions required by the GPL or the LGPL. If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under the terms of any one of the NPL, the GPL or the LGPL.
 *----------------------------------------------------------------------------*/

package com.targetx.modules;

import java.util.*;

import com.levelonelabs.aim.AIMAdapter;
import com.levelonelabs.aim.AIMBuddy;
import com.levelonelabs.aimbot.AIMBot;
import com.levelonelabs.aimbot.BotModule;
import java.util.logging.Logger;


/**
 * Handles requests for user queries
 *
 * @author Scott Oster
 *
 * @created September 6, 2002
 *
 * @todo instead of string, store HistoryObject (signon/off, date), then can easily compute on/off
 *       line durations
 * @todo attempt to remove signons caused by bot being offline and coming back
 * @todo when get to HistObjs attempt to maintain sanity of off/on/off/on cycle
 */
public class TargetxAdminModule extends BotModule {
	private static ArrayList services;
	private static final int SIZE = 6;
	static Logger logger=Logger.getLogger(TargetxModule.class.getName());

	/**
	 * Initialize the service commands. 
	 */ 
	static {
		services = new ArrayList();
		services.add("sys/info");
		services.add("sys/users");
		services.add("enemies");
		services.add("sys/signoff");
		services.add("sys");
	}

	private Hashtable userHash = new Hashtable();


	/**
	 * Constructor for AdminModule.
	 * 
	 * @param bot
	 */
	public TargetxAdminModule(AIMBot bot) {
		super(bot);
		//register to here about signon events
		super.addAIMListener(new AIMAdapter() {
			public void handleBuddySignOn(AIMBuddy buddy, String info) {
				handleBuddyEvent(buddy, "Signed on: ");
			}


			public void handleBuddySignOff(AIMBuddy buddy, String info) {
				handleBuddyEvent(buddy, "Signed off: ");
			}
		});
	}


	void handleBuddyEvent(AIMBuddy buddy, String type) {
		ArrayList arr = (ArrayList) userHash.get(buddy.getName().toLowerCase());
		if (arr == null) {
			arr = new ArrayList();
		}
		arr.add(type + " " + new Date());
		while (arr.size() > SIZE) {
			arr.remove(0);
		}
		userHash.put(buddy.getName().toLowerCase(), arr);
	}


	/**
	 * @see com.levelonelabs.aimbot.BotModule#getName()
	 */
	public String getName() {
		return "Admin Module";
	}


	/**
	 * @see com.levelonelabs.aimbot.BotModule#getServices()
	 */
	public ArrayList getServices() {
		return services;
	}


	/**
	 * @see com.levelonelabs.aimbot.BotModule#help()
	 */
	public String help() {
		StringBuffer sb = new StringBuffer();
		sb.append("<B>sys/info<i>USER</i></B> (displays user's recent sign on and off history)\n");
		sb.append("<B>sys/users</B> (displays status of all users)\n");
		sb.append("A=" + AIMBot.ROLE_ADMINISTRATOR + ", M = messages pending, * = new user, E = Enemy, - = banned\n");
		//sb.append("<B>enemies</B> (displays status of all enemies)\n");
		return sb.toString();
	}


	/**
	 * @see com.levelonelabs.aimbot.BotModule#performService(AIMBuddy, String)
	 */
	public void performService(AIMBuddy buddy, String query) 
	{
	    logger.info("In " + getName());
		String sql_str="";
		
		if (query.toLowerCase().startsWith("sys/info")) {
			StringTokenizer st = new StringTokenizer(query.trim(), " ");

			//check for right number of arguments
			if (st.countTokens() < 2) {
				sendMessage(buddy, "ERROR:\n" + help());
				return;
			}
 
			//grab the command and target
			String imcommand = st.nextToken();
			if (!imcommand.toLowerCase().equals("sys/info")) {
				sendMessage(buddy, "ERROR:\n" + help());
				return;
			}
			String imcommandTo = st.nextToken();
			AIMBuddy to = getBuddy(imcommandTo);

			//verify they are a user of the bot
			if (to == null) {
				sendMessage(buddy, "User " + imcommandTo
					+ " does not exist in the system.\nUse the ADDUSER command to add them.");
				return;
			}
			ArrayList hist = (ArrayList) userHash.get(to.getName().toLowerCase());
			if (hist == null) {
				sendMessage(buddy, "Sorry, I have no history for user: " + to.getName());
				return;
			} else {
				String result = "";
				for (Iterator iter = hist.iterator(); iter.hasNext();) {
					String element = (String) iter.next();
					result += (element + "\n");
				}
				sendMessage(buddy, result);
				return;
			}
		} else if (query.toLowerCase().trim().equals("sys/users")) {
			String result = "";
			int num = 0;

			for (Iterator iter = getBuddyNames(); iter.hasNext();) {
				String name = (String) iter.next();
				AIMBuddy bud = getBuddy(name);
				if (bud == null) {
					continue;
				}
				num++;
				String markup = "";
				if (userHash.get(name.toLowerCase()) == null) {
					markup += "*";
				}
				if (bud.hasRole(AIMBot.ROLE_ADMINISTRATOR)) {
					markup += "A";
				}
				if (bud.hasRole(AIMBot.ROLE_ENEMY)) {
					markup += "E";
				}
				if (bud.isBanned()) {
					markup += "-";
				}
				/*if (bud.hasMessages()) {
					markup += "M";
				}*/
				  
				if( bud.expHasMessages(bud,this.bot.getImId()) > 0)
				{
				   markup += "M";
				}
				
				result += (bud.getName() + "[" + (bud.isOnline() ? "<B>ON</B>" : "<I>OFF</I>") + "]" + markup + "    ");
			}
			if (result.trim().equals("")) {
				sendMessage(buddy, "Sorry, I have no information about active users.");
				return;
			} else {
				sendMessage(buddy, "Current (<b>" + num + "</b>) Users are:\n" + result);
				return;
			}
	    }
		else if (query.toLowerCase().trim().equals("sys/ping"))
		{
		   sendMessage(buddy, "1");
		}
		else if (query.toLowerCase().trim().equals("sys/status"))
		{
		  
		   String start_time = this.bot.getStartTime();
		   Integer tot_days=0;
		   String tot_hour="";
		   String tot_minute="";
		   String dis_string="";
		   
		   sql_str = "SELECT day(DATEDIFF(now(),'"+ start_time+"'))  as tot_days "+
                    ",hour(TIMEDIFF(now(),'"+start_time+"')) as tot_hour "+
                    ",minute(TIMEDIFF(now(),'"+start_time+"')) as tot_minute";
    		  
			      
    		   this.bot.test.RunSql(sql_str);   
        	   try 
               {
            	    this.bot.test.rs.next();
					tot_days   = this.bot.test.rs.getInt(1);
					tot_hour   = this.bot.test.rs.getString(2);
					tot_minute = this.bot.test.rs.getString(3);
            	  
        		}
                catch (Exception e)  
                {   
                       //logger.info("Error in menu module 1-- "+sql_str);      
                }
				
				dis_string = "uptime "+ tot_days+ " Days "+ tot_hour +" Hour "+tot_minute+ " Minute <BR>";
				
				// get the count of active members			
				String result = "";
			    int num = 0;

        			for (Iterator iter = getBuddyNames(); iter.hasNext();) 
					{
        				String name = (String) iter.next();
        				AIMBuddy bud = getBuddy(name);
        				if (bud == null) {
        					continue;
        				}
        				
        				String markup = "";
        				
        				if(bud.isOnline() )
						{
						  num++;
						}
        			}
				 dis_string += " Visitors "+ num;   
				
				// get the count of messages	
				int message_count = buddy.expHasMessages(buddy,this.bot.getImId());
				dis_string += "<BR> Messages "+message_count; 
		   
		   sendMessage(buddy,dis_string);
		}
		else if (query.toLowerCase().trim().startsWith("sys/loglevel"))
		{ 	
		    String logLevel ="";
		    String logFile  ="";
		    StringTokenizer st = new StringTokenizer(query.trim(), " ");
			//check for right number of arguments
			if (st.countTokens() < 2) 
			{
				sendMessage(buddy, "Please enter the correct format like. sys/loglevel INFO ");
				return;
			}

			/*
			//grab the command and target
			String imcommand = st.nextToken();
			if (!imcommand.toLowerCase().equals("sys/info")) {
				sendMessage(buddy, "ERROR:\n");
				return;
			}
			*/
			logLevel = st.nextToken();
			logLevel  = st.nextToken().trim();
			
			
			sql_str = "select log_file "+
	            " from im_clients where im_id ="+ this.bot.getImId() +"";
    	    bot.test.RunSql(sql_str);
            if(this.bot.test.totalNumRows >=1)
	        {
                try 
                {   
                   bot.test.rs.next();
                   if( bot.test.rs.getString(1).length() > 0)
                   {
                     
    		           logFile      =  bot.test.rs.getString(1).trim()+this.bot.getScreenName()+"_aimbot.log.xml";
    				   
                   }
                }  
                catch (Exception e) 
                {          
                          logger.info("Error in Admin module -- "+sql_str);
                          e.printStackTrace();
                }
	        }
			else
			{
			    sendMessage(buddy, "Log file name not specified in client table");
			
			}
		//logger.info("log sql -- "+logFile+"---"+logLevel);	
		  
		   this.bot.setupLogger(logLevel,logFile);
	  }
	  else if (query.toLowerCase().trim().startsWith("sys/sync"))
	  { 	
		  this.bot.expsync();
	  }
	  
	}
	
}