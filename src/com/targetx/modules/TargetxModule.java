package com.targetx.modules;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.levelonelabs.aim.AIMBuddy;
import com.levelonelabs.aimbot.AIMBot;
import com.levelonelabs.aimbot.BotModule;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/*
 * Handles requests to get Targetx info
 */
 
public class TargetxModule extends BotModule 
{
    static Logger logger=Logger.getLogger(TargetxModule.class.getName());
    private static ArrayList services;
    
    /**
     * Initialize the service commands.
     */
    static 
	{ 
        services=new ArrayList();
        services.add("targetx");  
    } 
	
	 /**
     * Constructor for TargetxModule.
     *
     * @param bot
     */
    public TargetxModule(AIMBot bot) {
        super(bot);
    }

    /**
     * @see com.levelonelabs.aimbot.BotModule#getName()
     */
    public String getName() {
        return "Targetx Module";
    }

	
	
    /**
     * @see com.levelonelabs.aimbot.BotModule#help()
     */
    public String help() {
        StringBuffer sb=new StringBuffer();
        sb.append(
            "<B>Climate <i>ZIPCODE</i></B> (displays the 5 day forcast for the specified zipcode)\n");
        sb.append(
            "* If the preference \"zipcode\" is set, you can omit the zipcode to use your default.");
        return sb.toString();
    }


    /**
     * @see com.levelonelabs.aimbot.BotModule#getServices()
     */
    public ArrayList getServices() {
        return services;
    }
	
	 /**
     * Return the list name
     *
     * @param buddy the buddy
     * @param query the list id
     */
    public void performService(AIMBuddy buddy, String query) 
	{
        if(query.trim().toLowerCase().startsWith("targetx")) 
		{
            StringTokenizer st=new StringTokenizer(query, " ");
            String list_def="12000001";
            if((list_def == null) && (st.countTokens() < 2)) 
			{
                super.sendMessage(buddy, "ERROR:\n"+help());
            } else 
			{
                String imcommand=st.nextToken();
                String list_id="";
                if(st.hasMoreElements()) 
				{
                    list_id =((String) st.nextElement()).trim();
                } else if(list_def != null) 
				{
                    list_id =list_def;
                }
                String result="Couldn't get database  information";
				Statement stmt = null;
                ResultSet rs = null;
				
                if(list_id.length() == 8) 
				{
				
				
				    try 
    				{
                         // The newInstance() call is a work around for some
                         // broken Java implementations
            
                         Class.forName("com.mysql.jdbc.Driver").newInstance();
                     } catch (Exception ex) 
            		 {
                         // handle the error
                     }
				
				    try {
                     Connection conn = DriverManager.getConnection("jdbc:mysql://192.168.5.30/express6?user=nobody");
                       stmt = conn.createStatement();
                          rs = stmt.executeQuery("SELECT  name  FROM lists where list_id="+list_id);
                          rs = stmt.getResultSet();
		             
        
                     } catch (SQLException ex) {
                         // handle any errors
                         //System.out.println("SQLException: " + ex.getMessage());
                         //System.out.println("SQLState: " + ex.getSQLState());
                         //System.out.println("VendorError: " + ex.getErrorCode());
                     }
					 
					 finally
					 {
					 
					       if (rs != null) 
						   {
                                 try 
    							 {
                                     String list_name=null;
									 rs.first();
									 list_name = rs.getString(1);
									 result= "Couldn't get database  information -" +list_name;
									 rs.close();
                                 } catch (SQLException sqlEx) 
								 { 
								    //System.out.println("SQLException: " + sqlEx.getMessage());
                                    // System.out.println("SQLState: " + sqlEx.getSQLState());
                                     //System.out.println("VendorError: " + sqlEx.getErrorCode());
    						     }
                        
                                 rs = null;
                           }
						   
						   
					 
					 }
				
				
                     
                }
                super.sendMessage(buddy, result);
            }
        }
    } 
    
	
	  public static void main(String[] args) 
	  {
        TargetxModule mod= new TargetxModule(null);
        String list_id = args[0];
        String result="Couldn't get weather information";
        if(list_id.length() == 8) 
		{
            result="Couldn't get weather information";
        }
        //System.out.println(result);
		
		
		
		
	/*	public class LoadDriver 
		{
            public static void main(String[] args) 
			{
                try 
				{
                     // The newInstance() call is a work around for some
                     // broken Java implementations
        
                     Class.forName("com.mysql.jdbc.Driver").newInstance();
                 } catch (Exception ex) 
        		 {
                     // handle the error
                 }
           }
 
      }*/
		
		
		
	}
	
	
	
	
	
	
}
