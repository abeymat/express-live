package com.targetx.modules;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.levelonelabs.aim.AIMBuddy;
import com.levelonelabs.aimbot.AIMBot;
import com.levelonelabs.aimbot.BotModule;
  
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import com.targetx.ExpressDbMgr;

/*
 * Handles requests to get Targetx info
 */
 
public class TargetxCounselorModule extends BotModule 
{
    static Logger logger=Logger.getLogger(TargetxCounselorModule.class.getName());
    private static ArrayList services;
	public String previous_counselor="";
    public Integer oldStudnet=0; // Flag to indicate whether the student is a old student or a new student
    public String def_coun_message="";// Default counselor message
    /**
     * Initialize the service commands.
     */
    static 
	{ 
        services=new ArrayList();
        
		services.add("talk");
		
    } 
	
	 /**
     * Constructor for TargetxModule.
     *
     * @param bot
     */
    public TargetxCounselorModule(AIMBot bot) {
        super(bot);
    }

    /**
     * @see com.levelonelabs.aimbot.BotModule#getName()
     */
    public String getName() {
        return "Targetx Counselor  Module";
    }

	
	
    /**
     * @see com.levelonelabs.aimbot.BotModule#help()
     */
    public String help() {
        StringBuffer sb=new StringBuffer();
        sb.append(
            "<B>Climate <i>ZIPCODE</i></B> (displays the 5 day forcast for the specified zipcode)\n");
        sb.append(
            "* If the preference \"zipcode\" is set, you can omit the zipcode to use your default.");
        return sb.toString();
    }


    /**
     * @see com.levelonelabs.aimbot.BotModule#getServices()
     */
    public ArrayList getServices() {
        return services;
    }
	
	 /**
     * Return the list name
     *
     * @param buddy the buddy
     * @param query the list id
     */ 
    public void performService(AIMBuddy buddy, String query) 
	{
	    String module="";
		String sql_str="";
		//String previous_counselor="";
		String counselor_ser_qstn="";
		String screen_name = this.bot.getScreenName();
		String counselor_scr_nm="";
		String smart_counselor_search = "N";
		Integer counselor_id=0;	
		String result="";
		logger.info("In " + getName());
		
		//brianwmniles
		//michaelcrusi
		//tgtxexpresslive
     	//String imcommandTo = "tgtxexpresslive1";
    	//String imcommandTo = " janecounselor";
		
		 
		
    if(query.trim().toLowerCase().startsWith("talk"))
	{
	       sql_str = "select smart_srch from im_counselor_groups "+
		             " where im_id = " +this.bot.getImId() +
					 " and counselor_group_id = "+  buddy.getCounselorGroupId();
	       this.bot.test.RunSql(sql_str);	    
		   if(this.bot.test.totalNumRows >=1)
	       { 
        		try 
                {
            	      this.bot.test.rs.next();
    				  smart_counselor_search =  this.bot.test.rs.getString(1);
    			}
    			catch (Exception e) 
    			{
    			 	  //System.out.println("nulll");
    			}
	       }
		   
			// Check whether the student already chated with a counselor.
			sql_str =   "select A.screen_name,A.def_message "+
                        " from im_counselors as A,im_buddies_counselor as B,im_buddies as C "+
                        " where A.im_id = C.im_id " +
                        " and   A.counselor_id = B.counselor_id "+
						" and   A.im_id = B.im_id "+
						" and   B.buddies_id = C.buddies_id "+
                        " and   A.im_id ="+ this.bot.getImId() +
						" and   B.counselor_group_id = "+  buddy.getCounselorGroupId() +
                        " and   C.screen_name =  '" + buddy.getName() + "' " ;
						
    		   	   
    	    this.bot.test.RunSql(sql_str);
			
			if(this.bot.test.totalNumRows >=1)
	        {    
        		try 
                {
            	      this.bot.test.rs.next();
    				  AIMBuddy bud = getBuddy(this.bot.test.rs.getString(1));		
    				  // check if any available counselor is online
    				  if(smart_counselor_search.equals("Y"))
    				  {
                    	  if(bud.isOnline() )
                		  {
                		      this.previous_counselor     = this.bot.test.rs.getString(1);
    						  counselor_scr_nm            = this.bot.test.rs.getString(1);
                		  }
    				  }
    				  else if(smart_counselor_search.equals("N"))
    				  {
    				      this.previous_counselor     = this.bot.test.rs.getString(1);
    					  counselor_scr_nm            = this.bot.test.rs.getString(1);
                      }
    				  
    				  this.def_coun_message = this.bot.test.rs.getString(2);
    				  
    				  this.oldStudnet = 1;
        	  	}
    			catch (Exception e) 
    			{
    			 	  //System.out.println("nulll");
    			}
			}
			else
			{
			  this.previous_counselor ="";
			  counselor_scr_nm="";
			  this.def_coun_message="";
			  this.oldStudnet=0;
			}
			   if(this.previous_counselor == null)
			   {
			     this.previous_counselor ="";
			   }
			/*
    		catch (SQLException sqlEx) 
            {   
			     logger.info("in Talk module1 -- "+sql_str);
                         
            }	
			*/
			
			  
			
			// set the submodele to ""
		     buddy.setSubModule("");
			
			 if(counselor_scr_nm.length() > 1)
			 {
			     this.contactCounselor( buddy,counselor_scr_nm);
             }
              //logger.info("trace-1.2"); 
			
			// Look in the menu table for a search question	
			if(this.previous_counselor.length() < 1)
			{
			
			 	 sql_str =  "select  A.search_qstn FROM im_counselor_groups as A where A.im_id = " + this.bot.getImId() +
	                         "  and A.counselor_group_id = "+ buddy.getCounselorGroupId() ;						   	 
			     this.bot.test.RunSql(sql_str);
				 if(this.bot.test.totalNumRows >=1)
	             {
            		try 
                    {
                	      this.bot.test.rs.next();
                          counselor_ser_qstn = this.bot.test.rs.getString(1);
            	  	}
					catch (Exception e) 
			        {
			 	         //System.out.println("1---"+sql_str);
			        }
				}	
            		/*catch (SQLException sqlEx) 
                    {   
                                 
                    }	
					*/
					if(counselor_ser_qstn != null)
				    {
        				 //this.bot.expressPush("Talk1");
						 buddy.setSubModule("Talk1");
        		         //String result="Please enter your zip code.";
        		         super.sendMessage(buddy, counselor_ser_qstn);	
			         }
					 else
					 {
					   counselor_ser_qstn="";
					 }
					
					
			}
			// Get a counselor using Random picking		
			if(counselor_ser_qstn.length() < 1 & this.previous_counselor.length() < 1)
			{	
			   this.randomPickCounselor(buddy);
			}
	
		 
		}
		else if(buddy.getSubModule().startsWith("Talk1")  )
		{   
		     
		    // else if(this.bot.expressPeek().toString().startsWith("Talk1")  )
		    // else if(query.trim().toLowerCase().startsWith("19007") || query.trim().toLowerCase().startsWith("19053") )
		    //this.bot.expressPop();
			buddy.setSubModule("");
			/*
    		sql_str =  "select  A.screen_name FROM im_counselors as A where " +
			           " A.im_id = " + this.bot.getImId()   +
			           " and A.look_up like  ('%" +query.trim().toLowerCase()+ "%')";
	        */
			
			//sql_str = "set session ft_min_word_len=2";
			//test.RunSql(sql_str);		   
			sql_str = 	"select  A.screen_name,A.counselor_id,def_message FROM im_counselors as A where " +
			            " A.im_id = " + this.bot.getImId()   +
                        " and MATCH (look_up) AGAINST ('" + query.trim().toLowerCase() +"')";   
			   	   
    	    this.bot.test.RunSql(sql_str);
			
			//System.out.println("--t1-"+sql_str);
    		if(this.bot.test.totalNumRows >=1)
	        {  //System.out.println("--t2-");
        		try 
                {
            	      while (this.bot.test.rs.next())
    				  {
					   //System.out.println("--t3-"+this.bot.test.rs.getInt(2));
    				      AIMBuddy bud = getBuddy(this.bot.test.rs.getString(1));
    					  counselor_scr_nm = this.bot.test.rs.getString(1);
        				  counselor_id     = this.bot.test.rs.getInt(2);	
    					  if(bud.isOnline() )
                		  {
                		     counselor_scr_nm = this.bot.test.rs.getString(1);
        				     counselor_id     = this.bot.test.rs.getInt(2);
    						 break;
                		  }
    					  	
                          
        				  this.def_coun_message = this.bot.test.rs.getString(3);
    				 }
        	  	}
        		catch (Exception e) 
                {   
                     //System.out.println("3---");        
                }
		    }
	//System.out.println("--t4-"+counselor_id);
		
		/*
    		  AIMBuddy to = getBuddy(counselor_scr_nm);
    		  if (to.isOnline()) 
    		  {	   
            	if (buddy != null) 
            	{
            		String name = buddy.getName();
                }
    			  //sendMessage(buddy, "hey it is working");
            	  //sendMessage(to, " said: " );
				  module="proxy";
				  this.bot.ExpressStack.push("proxy");
    			  query = "proxy " + counselor_scr_nm;
    	          bot.handleMessage(buddy,query,module);
    		  }
    		  else
    		  {
    		  
    		    String result = counselor_scr_nm + " isn't available.  Would you like to leave a message? Type <b>yes</b> or <b>no</b>  " ;
    		    super.sendMessage(buddy, result);
				
				
				//super.sendMessage(buddy,  " please type <b>yes</b> or <b>no</b>" );
    	
    		  
    		 }
			 
			 */
			 
			 // if(this.oldStudnet != 1 & counselor_id > 0)
			  
			 if( counselor_id > 0)
			 {
			       
				     // Make an entry into buddies counselor table
				    buddy.addBuddiesCounselor(buddy,counselor_id,this.bot.getImId(),buddy.getCounselorGroupId());
				
				
				
			 }
			 //System.out.println("trace---"+counselor_scr_nm);
			 if( counselor_scr_nm.length() > 1)
			 {
			     this.contactCounselor(buddy,counselor_scr_nm);
			 }
			 else
			 {
			    // pick a counselor randomly
				this.randomPickCounselor(buddy); 
			 
			 }
			 
    	}
	/*	else if(query.trim().toLowerCase().startsWith("yes")) - commented on feb 12
		{
		
		       if( buddy.getCounselor().length()> 0)
			   {
			     counselor_scr_nm = buddy.getCounselor();
				 
			   }
		      
		    
		        module="tell";
    			query = "counselor " + counselor_scr_nm + " "  ;
    	        bot.handleMessage(buddy,query,module);
		
		}
		else if(query.trim().toLowerCase().startsWith("no"))
		{
			      
    			
    	        bot.handleMessage(buddy,"start","start");
			    if( buddy.getCounselor().length()> 0)
			   {
				    buddy.setCounselor("");
			    }
		
		} */
		else
		{    
		    //logger.info("in Talk module -- "+this.bot.expressPeek().toString());	
		   // result="I'm sorry, I do not understand what you wrote.  Please type <b> Start </b> to view a menu of options";
		   // super.sendMessage(buddy, result);
		   result =  this.bot.getDefaultMessage();
		   super.sendMessage(buddy, result);
		}
		
    } 
     
	
	  public void contactCounselor( AIMBuddy buddy,String counselor_scr_nm)
	  {
	   		  String module="";
		      String query="";
    		  AIMBuddy to = getBuddy(counselor_scr_nm);
			  buddy.setCounselor(counselor_scr_nm);
			 
			  // if The counselor is online and not in proxiInital ,then try to contact the counselor
    		  if (to.isOnline() && !to.getModule().equals("proxy")) 
    		  {	   
            	if (buddy != null) 
            	{
            		String name = buddy.getName();
                }
    			 
				  module="proxy";		  
    			  query = "proxy " + counselor_scr_nm;
    	          bot.handleMessage(buddy,query,module);
    		  }
    		  else
    		  {
    		  
    		    String result=""; 
				if( this.def_coun_message.length() > 1)
				{
				   result = this.def_coun_message +"<BR>";
				}
				else
				{
				    result = counselor_scr_nm + " isn't available.  Please leave a message  " ;   		  
				}
				super.sendMessage(buddy, result);
		        module="tell";
    			query = "counselor " + counselor_scr_nm + " "  ;
    	        bot.handleMessage(buddy,query,module);			
			  }
	  } 
	  
	  public void randomPickCounselor(AIMBuddy buddy)
	  {
	      String sql_str="";
		  String counselor_scr_nm=""; 
		  String offline_counselor_scr_nm="";
		  String def_counselor_scr_nm="";
		  int counselor_id=0;
		  
			   sql_str = " select screen_name,counselor_id,def_message,def_counselor FROM im_counselors WHERE im_id = " + this.bot.getImId() +
				          "  and counselor_group_id = "+buddy.getCounselorGroupId()  + " ORDER BY RAND( ) ";
				this.bot.test.RunSql(sql_str);
				if(this.bot.test.totalNumRows >=1)
	            {	 
    				try 
                    {
                	      while (this.bot.test.rs.next())
    					  {
    					     AIMBuddy bud = getBuddy(this.bot.test.rs.getString(1));		
    						 offline_counselor_scr_nm = this.bot.test.rs.getString(1);
    						 counselor_id     = this.bot.test.rs.getInt(2);
    						 // check if any available counselor is online
                    		 if(bud.isOnline() )
                		     { 
    						   offline_counselor_scr_nm ="";
    						   def_counselor_scr_nm      ="";
                		       counselor_scr_nm = this.bot.test.rs.getString(1);
    						   break; 
                		     }
    						 
    						 if(this.bot.test.rs.getString(4).equals("Y"))
    						 {
    						   def_counselor_scr_nm = this.bot.test.rs.getString(1);
    						 }
                             
        					 
        					 this.def_coun_message = this.bot.test.rs.getString(3);
    					  }
            	  	}
            		catch (Exception e) 
                    {   
                        
    				   e.printStackTrace();         
                    }	
				}
				if(counselor_scr_nm.length() < 1 )
				{
				  if(this.previous_counselor.length() > 1)
				  {
				    counselor_scr_nm = this.previous_counselor;
				    counselor_id=0;
				  }
				  else if(def_counselor_scr_nm.length() > 1)
				  {
				    counselor_scr_nm = def_counselor_scr_nm;
				  
				  }
				  else
				  {
				    counselor_scr_nm = offline_counselor_scr_nm;
				   
				  }
				
				
				}
				
				//System.out.println("second ........... trace---"+sql_str);
				
				if(this.oldStudnet != 1 & counselor_id > 0)
				{
				  
				    // Make an entry into buddies counselor table
				    buddy.addBuddiesCounselor(buddy,counselor_id,this.bot.getImId(),buddy.getCounselorGroupId());
				}
				
				
				 if(counselor_scr_nm.length() > 1)
    			 {
    			     this.contactCounselor( buddy,counselor_scr_nm);
                 }
				 
	  
	  }
	
	  public static void main(String[] args) 
	  {
        TargetxCounselorModule mod= new TargetxCounselorModule(null);
        String list_id = args[0];
        String result="Couldn't get weather information";
        if(list_id.length() == 8) 
		{
            result="Couldn't get weather information";
        }
        //System.out.println(result);
	}

}
