package com.targetx.modules;
  
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.levelonelabs.aim.AIMBuddy;
import com.levelonelabs.aimbot.AIMBot;
import com.levelonelabs.aimbot.BotModule;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import com.targetx.ExpressDbMgr;
  

/*
 * Handles requests to get Targetx info
 */
 
public class TargetxMenuModule extends BotModule 
{
    static Logger logger=Logger.getLogger(TargetxModule.class.getName());
    private static ArrayList services;
	public ArrayList counselor_group;
	public ArrayList counselor_keyword;
	public ArrayList possible_keyword;
	public int parent_id=0;
    /**
     * Initialize the service commands.
     */

    static 
	{ 
        services=new ArrayList();
        services.add("start");
		services.add("menu");
		services.add("hello");
		services.add("hi");
		services.add("visit");
		services.add("fees");
		services.add("app"); 
		
		
		
		
    } 
	
	 /**
     * Constructor for TargetxModule.
     *
     * @param bot
     */
    public TargetxMenuModule(AIMBot bot) {
        super(bot);
    }

    /**
     * @see com.levelonelabs.aimbot.BotModule#getName()
     */
    public String getName() {
        return "Targetx Menu Module";
    }

	
	
    /**
     * @see com.levelonelabs.aimbot.BotModule#help()
     */
    public String help() {
        StringBuffer sb=new StringBuffer();
        sb.append(
            "<B>Climate <i>ZIPCODE</i></B> (displays the 5 day forcast for the specified zipcode)\n");
        sb.append(
            "* If the preference \"zipcode\" is set, you can omit the zipcode to use your default.");
        return sb.toString();
    }


    /**
     * @see com.levelonelabs.aimbot.BotModule#getServices()
     */
    public ArrayList getServices() {
        return services;
    }
	
	 /**
     * Return the list name
     *
     * @param buddy the buddy
     * @param query the list id
     */
    public void performService(AIMBuddy buddy, String query) 
	{
	    logger.info("In " + getName());
	    String module="";
		String sql_str="";
		String sql_str1="";
		String sql_str2="";
	    String screen_name;
	    screen_name = this.bot.getScreenName();
		String dis_menu="";
		String im_name=this.bot.getImName();
		
		
		/** --- My code ----  **/

		sql_str =  "select  A.counselor_id  FROM im_counselors as A where A.im_id = " 
	           + this.bot.getImId() + " and  A.screen_name = '"+ buddy.getName()    + "' and A.active = 'Y'"  ;	   
	    this.bot.test.RunSql(sql_str);
		if(this.bot.test.totalNumRows >=1)
	    {
    		try 
            {
        	      this.bot.test.rs.next();
                  if( this.bot.test.rs.getInt(1) > 0)
    			  {
    			    buddy.setIsCounselor(this.bot.test.rs.getInt(1));
    			  
    			  }
    	  	}
    		catch (Exception e) 
            {   
    		                buddy.setIsCounselor(0);
                            //logger.info("Error in menu module -- "+sql_str);
            }
		}
		else
		{
		        buddy.setIsCounselor(0);
		}
		this.bot.test.CloseResultSet();
		
		counselor_group = new ArrayList();
		counselor_keyword = new ArrayList();
		possible_keyword  = new ArrayList();
		if(counselor_group.size()< 1 & this.parent_id == 0)
		{
		     
			 sql_str =  "select  keyword,counselor_group_id FROM im_menu where im_id = " +this.bot.getImId()+
			            "  and  parent_id = 0  and counselor_group_id > 0 and active = 'Y' order by seq_no"; 
			 
			 //logger.info("here sql -- "+sql_str);
			 this.bot.test.RunSql(sql_str);
			if(this.bot.test.totalNumRows >=1)
	        { 
        		try 
                {   
            	      while (this.bot.test.rs.next())
    				  { 
                          if( this.bot.test.rs.getString(1).length() > 0 )
            			  {
        				    
                			    counselor_group.add(this.bot.test.rs.getInt(2));
                			    counselor_keyword.add(this.bot.test.rs.getString(1).toLowerCase().trim());	
								//logger.info("here 1 -- "+this.bot.test.rs.getInt(2));	
								//logger.info("here 2 -- "+this.bot.test.rs.getString(1).toLowerCase().trim());	 
    					  }
    					  
    				  }
        	  	}
        		catch (Exception e) 
                {   
        		      logger.info("Error in menu module -- "+sql_str);
                }
    		}
    		this.bot.test.CloseResultSet();
		}
		
		if(possible_keyword.size()< 1)
		{ 
			sql_str =  "select  keyword,counselor_group_id FROM im_menu where im_id = " +this.bot.getImId()+
			            "  and   active = 'Y'  and counselor_group_id =0 and parent_id = "+this.parent_id +" order by seq_no"; 			
                        //  "  and  parent_id in (select distinct seq_no from im_menu where im_id = " +this.bot.getImId()+ " and parent_id = 0 ) and active = 'Y' order by seq_no";
			this.bot.test.RunSql(sql_str);
			if(this.bot.test.totalNumRows >=1)
	        { 
        		try 
                {  
            	      while (this.bot.test.rs.next())
    				  {  
                          if( this.bot.test.rs.getString(1).length() > 0 )
            			  {
        				    
        					     possible_keyword.add(this.bot.test.rs.getString(1).trim().toLowerCase());
    							 //logger.info("keyword -- "+this.bot.test.rs.getString(1));
    						  
    					  }
    					  
    				  }
        	  	}
        		catch (Exception e) 
                {   
        		      logger.info("Error in menu module -- "+sql_str);
                }
    		}
    		this.bot.test.CloseResultSet();
		
        			  
			
		}
		 
		             
		sql_str="";
		
		/**   End code **/
		
		if( buddy.getIsCounselor() < 1)
		{
	   
                if(query.trim().toLowerCase().startsWith("start") || query.trim().toLowerCase().startsWith("hello") || query.trim().toLowerCase().startsWith("hi") || query.trim().toLowerCase().startsWith("menu")) 
        		{
        			   sql_str =  "select  keyword,key_desc,seq_no,parent_id,counselor_group_id FROM im_menu as A where A.im_id =" 
        	           + this.bot.getImId() + "  and parent_id = 0 and keyword <> 'def'  and A.active = 'Y' order by seq_no"; 
        			   dis_menu="<br><b><u>Welcome to " + im_name + " </u></b><br>";
                }
        		else if(possible_keyword.contains(query.trim().toLowerCase()))
        		{
        			/*   sql_str =  "select  keyword,key_desc,seq_no FROM im_menu as A,im_clients as B where B.screen_name = '" 
        	           + screen_name + "' and   A.im_id = B.im_id  and A.keyword ='"+ query.trim().toLowerCase() +"' order by seq_no";
        		    */
					/*
					sql_str =  "select  keyword,key_desc,seq_no,parent_id FROM im_menu where im_id ="+ this.bot.getImId()+ " and "+
					" parent_id in (select distinct menu_id from im_menu where im_id = " +this.bot.getImId()+ " and keyword = '"+ query.trim().toLowerCase() +"' )"+ "and active = 'Y' order by seq_no";
				    */
					sql_str =  "select  keyword,key_desc,seq_no,if(length(keyword)<1,menu_id,parent_id) as parent_id FROM im_menu where im_id ="+ this.bot.getImId()+ " and "+
					" parent_id in (select distinct menu_id from im_menu where im_id = " +this.bot.getImId()+ " and keyword = '"+ query.trim().toLowerCase() +"' )"+ "and active = 'Y' order by seq_no";
				    this.bot.test.RunSql(sql_str);

                	   if(this.bot.test.totalNumRows < 1)
            	       {
					       sql_str =  "select  keyword,key_desc,seq_no,if(length(keyword)<1,menu_id,parent_id) as parent_id FROM im_menu where im_id ="+ this.bot.getImId()+ " and "+
					      " parent_id = " + this.parent_id + " and keyword = '"+ query.trim().toLowerCase() +"' "+ "and active = 'Y' order by seq_no";
					   }
				
				
				}
        		else if(counselor_keyword.contains(query.trim().toLowerCase()))
        		{
				    // Get the group Id
					int key_index = counselor_keyword.indexOf(query.trim().toLowerCase());
				    int group_id = (Integer)counselor_group.get(key_index);
					 
					
				    if(group_id > 0)
					{
					   buddy.setCounselorGroupId(group_id);
					}
				
        		    module="talk";
        	        bot.handleMessage(buddy,"talk",module);
					return;
        		}
        		else
        		{
        		  /*	sql_str =  "select  keyword,key_desc,seq_no,"+this.parent_id+" FROM im_menu as A where A.im_id = " 
        	           + this.bot.getImId() + " and    A.keyword ='def' order by seq_no";
					*/
        		}
        }
		else if (buddy.getIsCounselor() > 0)
		{
		    dis_menu="<br><b><u>Welcome to " + im_name + " </u></b><br>"+
			         "Type <b>InBox </b>to check the messages<br>"
					 ;
			buddy.setModule("show");
		
		} 		
		
		 /** --- My code ----  **/
	 
	 
	 if(sql_str.length() > 5)
	 {
    	   this.bot.test.RunSql(sql_str);
    	   if(this.bot.test.totalNumRows >=1)
	       {
        	   try 
               {
            	   while (this.bot.test.rs.next())
            	   {
		   
				     if(this.bot.test.rs.getString(2) == "NULL")
					 {
					   super.sendMessage(buddy, "No description for keyword");
					   return;
					 }
                      dis_menu = dis_menu + " <br> " + this.bot.test.rs.getString(2);
					  this.parent_id = this.bot.test.rs.getInt(4);
            	   }
        		}
                catch (Exception e)  
                {        
                }
    	   }
		   else
		   {
		      //dis_menu = "Sorry the key word "+ query + " is not defined ";
		     // super.sendMessage(buddy, dis_menu);
		   }
		    this.bot.test.CloseResultSet();
			
		
          
	  }
	  
	  if(dis_menu.length() > 1)
      {     
    	super.sendMessage(buddy, dis_menu);
      }
	  else
	  {
	    dis_menu = this.bot.getDefaultMessage();
		super.sendMessage(buddy, dis_menu);
	       
	  }
	  
	  
	  
    } 
    
	
	  public static void main(String[] args) 
	  {
        TargetxModule mod= new TargetxModule(null);
        String list_id = args[0];
        String result="Couldn't get weather information";
        if(list_id.length() == 8) 
		{
            result="Couldn't get weather information";
        }
        //System.out.println(result);
		
		
		
		
	
		
		
	}
	
	
	
	
	
	
}
