package com.targetx;

import snaq.db.*;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSetMetaData;
import java.util.logging.Logger;
  
public class ExpressDbMgr
{
    public static Connection db_conn;
    public static String test;
	public String[][] result_1=  new String[1][10];
	public ResultSet rs = null;
	public int totalNumRows;
	ConnectionPoolManager cpm = null;
	public int ErrorNo;
	public Logger logger = Logger.getLogger("LL");
	
	public  ExpressDbMgr()
	{
	 	try
    	{   
		    
    		 //this.cpm = ConnectionPoolManager.getInstance("dbpool.properties");
			 this.cpm = ConnectionPoolManager.getInstance("dbpool.properties");
    	}
    	catch (IOException ioe)
    	{
		     
    		 this.ErrorNo = -1000;
    	}
	
	}
	
	public void ReleasePool()
	{
	    this.cpm.release();
	
	}
	
	public void ExpressLog(SQLException sqlEx,String sqlStr )
	{
	    logger.info("Initializing " + " in the beginning");
		logger.info("QLException:c" + sqlStr);
		logger.info("QLException:c" + sqlEx.getMessage());
        logger.info("SQLState: "    + sqlEx.getSQLState());
		logger.info("VendorError: " + sqlEx.getErrorCode());
        //System.out.println("VendorError: " + sqlEx.getErrorCode());
	}

    public  void init()
	{
	
	Statement stmt = null;
    ResultSet rs = null;
	String result=null;
	String list_id="12000001";
	
	long timeout = 2000;

	try
	{
	   
	   this.db_conn =  this.cpm.getConnection("local",timeout);

	}
	catch (SQLException sqle)
	{
	    
	     this.ErrorNo = -1001;

	}
 
	
    } //end main
	
	
	
	public void RunSql(String sql_str)
	{
	
	  Statement stmt = null;
      //ResultSet rs = null;
	  String result=null; 
	  
	    try 
        {   
            if(this.db_conn.isClosed())
    		{
    		   this.init();
    		}   
        }
		catch (Exception e) 
        {              
             
              e.printStackTrace();
        }
	  
	  
	  if (this.db_conn != null)
	  {
    	    if( sql_str.trim().toLowerCase().startsWith("select"))
    		{  
                	  try
            		  { 
                    		 stmt = db_conn.createStatement();
							 rs = stmt.executeQuery(sql_str);
                             rs = stmt.getResultSet();
            				 rs.last();
            				 totalNumRows = rs.getRow();
            				
            				 rs.beforeFirst();
            			 }
            			 catch (SQLException sqle)
                    	{
						   this.ErrorNo = -1002;
                    	   logger.severe("In valid sql" + sql_str);
                    	}
						
						
			}
		    else // update or insert
			{
			           try
            			{ 
                    		 stmt = db_conn.createStatement();
            				 //System.out.println(sql_str);
                             totalNumRows = stmt.executeUpdate(sql_str);                             
            				 //System.out.println("Total no of rows updateed - " + totalNumRows );
            			 }
            			 catch (SQLException sqle)
                    	{
						   this.ErrorNo = -1002;
                    	}

			
			}
	  }
	  else
	  {
	     
	  }
	  
	 
	}
	
	public void GetRow()
	{
	   if (rs != null) 
	   {
	          
			  
               try 
               {
                     String list_name=null;
					 rs.next();
					 ResultSetMetaData rsmd = rs.getMetaData();
                     int numColumns = rsmd.getColumnCount();

					  for (int j=1; j< numColumns+1; j++) 
                      {

					     result_1[0][j] = rs.getString(j);

					  }
                	 
                 } catch (SQLException sqlEx) 
                 {   this.ErrorNo = -1003;
                     
                  }
                    
        }
		
	    	
	
	
	}
	

	
	public void CloseResultSet()
	{
	   if (rs != null) 
	   {
	
    	   try
    	   {
        	   this.rs.close();
    	   }
    	   catch (SQLException sqlEx) 
           { 
		     this.ErrorNo = -1004;
             this.ExpressLog(sqlEx,"");
           }
	    }
	}

	public void CloseDbConnection()
	{
	   
	  try 
	  { 
	     this.db_conn.close(); 
	  }
	  catch (Exception e) 
	  //catch (SQLException sqlEx) 
	  {
	    
	  }
	}

} //end class
